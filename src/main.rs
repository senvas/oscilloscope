
extern crate lazy_static;
extern crate neith;
extern crate neith_widgets;

use neith::com::MutVal;
use neith::config::Color;
use neith::layout::HorizontalAlign;
use neith::layout::VerticalAlign;
use neith::math::Vector2;
use neith::widget::Style;
use neith::widget::Widget;
use neith::{
    layout::split::*,
    layout::Direction,
};

extern crate neith_marp;
use neith_widgets::graph::{LineGraph, GraphHost};
use std::process::Command;

use std::sync::{Arc, RwLock};
use std::fs::File;
use std::io::prelude::*;

use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        panic!("Please specify input device!");
    }

    let device_path = args[1].clone();

    Command::new("stty").arg("-F").arg(device_path.clone()).arg("raw")
        .output()
        .expect("Failed to set UART raw mode");
    Command::new("stty").arg("-F").arg(device_path.clone()).arg("-echo")
        .output()
        .expect("Failed to disable UART echo");
    Command::new("stty").arg("-F").arg(device_path.clone()).arg("-ispeed").arg("3000000000")
        .output()
        .expect("Failed to set baud UART rate");

    let mut file = File::open(&device_path).expect("invalid path!");

    let buf_size = 1024;
    let live_graph_data = neith::com::MutVal::new( vec![(0.0, 0.0); buf_size] );

    //(*live_graph_data.get_mut())[buf_size] = (0.0, 0.0);
    //(*live_graph_data.get_mut())[buf_size+1] = (buf_size as f32, 1.0);

    std::thread::spawn(
        {
            let b = live_graph_data.clone();
            move || {
                let mut idx = 0;
                loop {
                    let mut bytes = [0 as u8; 2];
                    file.read_exact(&mut bytes);

                    let val = ((bytes[0] as u16) << 8) | (bytes[1] as u16);

                    b.get_mut()[idx] = (idx as f32, val as f32);
                    idx = ( idx + 1 ) % buf_size;
                }
            }
        }
    );

    neith_marp::new_interface(None, None, move |mut interface| {

        let capture_graph_data = neith::com::MutVal::new( Vec::new() );

	let should_end = Arc::new(RwLock::new(false));

	let split = neith::layout::Split::new()
	    .with_direction(Direction::Horizontal)
	    .with_moveable(false)
            .with_split_type(neith::layout::split::SplitType::Absolut(50.0))
	    .with_one(
                neith::layout::Split::new()
                    .with_direction(Direction::Vertical)
	            .with_moveable(false)
                    .with_split_type(neith::layout::split::SplitType::Relative(0.5))
	            .with_one( neith::io::Label::new(&device_path) )
                    .with_two( neith::io::Button::new("Capture").with_action({
                        let live_data = live_graph_data.clone();
                        let capture_data = capture_graph_data.clone();
                        move || {
                            *capture_data.get_mut() = live_data.get().clone();
                        }
                    }) )
            )
            .with_two(
                neith::layout::Split::new()
                    .with_direction(Direction::Horizontal)
	            .with_moveable(false)
                    .with_split_type(neith::layout::split::SplitType::Relative(0.5))
	            .with_one( GraphHost::new().with_graph( LineGraph::new().with_mut_val( live_graph_data.clone() ) ) )
                    .with_two( GraphHost::new().with_graph( LineGraph::new().with_mut_val( capture_graph_data.clone() ) ) )
            );

	//Setup standard interface
	let root = interface.root().clone();
	root.write().expect("Could not lock root").set_widget(split);

	let start = std::time::Instant::now();

	while !interface.update() {

            
	}
    });
}
